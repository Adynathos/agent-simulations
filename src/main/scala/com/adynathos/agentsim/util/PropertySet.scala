package com.adynathos.agentsim.util

import sim.util.Properties
import scala.collection.mutable
import scala.reflect.ClassTag
import scala.reflect.runtime.universe._
import java.lang.reflect.Field

class PropertyDefinition(val title : String, val valueType : Class[_],
		val field : Field, val getter : () => Object, val setter : Object => Object,
		val domain : Object) {
}

//class PropertyDefinitionAutoTyped[T <: Object](name : String, gettera : () => T, implicit val tp : ClassTag[T]) 
//	extends PropertyDefinition(name, tp.runtimeClass, gettera, null)
//		


trait PropertySet extends Properties {
	protected val properties = new mutable.ArrayBuffer[PropertyDefinition]
	
	/** Properties interface **/
	def getName(idx: Int) = properties(idx).title
	override def getDescription(idx : Int) = "prop " + properties(idx).title
	def getType(idx: Int) = properties(idx).valueType
	override def getDomain(idx : Int) = {
		println("get domain " + properties(idx).title + " -> " + properties(idx).domain)
		properties(idx).domain
	}
	def getValue(idx: Int) = {
		println("get " + properties(idx).title + " -> " + properties(idx).getter())
		properties(idx).getter()
	}
	override def setValue(idx: Int, value : Object) = {
		println("set " + properties(idx).title + " to " + value)
		properties(idx).setter(this, value) 
	}
	override def _setValue(idx: Int, value : Object) = {
		println("_set " + properties(idx).title + " to " + value)
		setValue(idx, value)
	}
	def isReadWrite(idx: Int) = true
	def isVolatile = false
	def numProperties = properties.size
	/** end Properties interface **/
	
	//def addProperty(propertyDef : PropertyDefinition) = properties += propertyDef
	def addProperty(name : String, title : String, domain : Object) = {
		val attribute = getClass.getDeclaredField(name)
		
		// This is my data and i will do what i want with it!
		if(!attribute.isAccessible()) attribute.setAccessible(true)
				
		properties += new PropertyDefinition(title, attribute.getClass,
				attribute,
				() => attribute.get(this),
				(x : Object) => { attribute.set(this, x); println("set to " + x);  x },
				domain)
	}
}