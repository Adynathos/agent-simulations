import org.jfree.data.general.SeriesChangeListener
import org.jfree.chart.renderer.category.BarRenderer

package sim.util.media.chart {
	class FixedBarChartSeriesAttributes(generator : ChartGenerator, name : String, index : Int, stoppable : SeriesChangeListener)
		extends BarChartSeriesAttributes(generator, name, index, stoppable)
	{
		override protected def getElements = {
			if (elements != null) elements
			else if (elements2 != null) elements2.toArray()
			else null
		}
	}

	class FixedBarChartGenerator extends BarChartGenerator {
		override protected def buildNewAttributes(name : String, stopper : SeriesChangeListener) = {
			new FixedBarChartSeriesAttributes(this, name, getSeriesCount(), stopper)
		}
	}

	// Bar chart that is stacked by default -- no way to implement that
	class StackedBarChartGenerator extends FixedBarChartGenerator {

	}
}
