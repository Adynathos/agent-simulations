package com.adynathos.agentsim

import students.StudentsUI
import segregation.SegregationUI
import forum.ForumUI
import sim.display.Console

object Main extends App {
	val console = new Console(new ForumUI())
	console.setPlaySleep(10)
	console.setVisible(true)
}
