package com.adynathos.agentsim.forum

import sim.engine.Steppable
import sim.engine.SimState
import sim.field.network.Edge

class ForumAgent(val topic : Int) extends Steppable {
	var server : ForumServer = null
	
	override def step(untypedState : SimState) = untypedState match {
		case state : ForumState => {
			if(state.friendCount > 0 && state.randFriendProb) {
				findNewFriend(state)
			}
			
			if(! isSatisfied(state) && state.randSwitchProb) {
				// by default to random server
				var to_server = state.randomServer
				
				// probability to ask friend for advice and go to their server
				if(state.randRecommendationProb) {
					val edges_out = state.friendsNet.getEdgesOut(this)
					
					if(edges_out.size > 0) {
						val friend = state.randomSampleBag[Edge](edges_out).asInstanceOf[ForumAgent]
						to_server = friend.server
					}
				}
				
				moveToServer(to_server)
			}
		}
		case _ => {}
	}
	
	def findNewFriend(state : ForumState) = {
		// we will be finding a new friend
		
		// get new friend
		
		var attempts = 10
		var new_friend : ForumAgent = null
		while(attempts > 0) {
			new_friend = server.randomFriend(this, state)
			
			if(state.friendsNet.getEdge(this, new_friend) != null) {
				state.friendsNet.addEdge(this, new_friend, null)
				attempts = 0
			}
			
			attempts -= 1
		}
			
		removeExcessiveFriends(state)
		new_friend.removeExcessiveFriends(state)
	}
	
	def removeExcessiveFriends(state : ForumState) = {
		// check current friend list and remove if too many
		val edges_out = state.friendsNet.getEdgesOut(this)
		while(edges_out.size > state.friendCount) {
			val to_remove = state.randomSampleBag[Edge](edges_out)
			state.friendsNet.removeEdge(to_remove)
		}		
	}
	
	def moveToServer(new_server : ForumServer) = {
		if(server != null) {
			server.removeAgent(this)
		}
		
		server = new_server
		
		if(server != null) {
			server.addAgent(this)
		}
	}
	
	def isSatisfied(state : ForumState) = 
		server != null && server.topicPopularity(topic) >= state.minority
	
}