package com.adynathos.agentsim.forum

import sim.display.Console
import sim.display.Controller
import sim.display.Display2D
import sim.display.GUIState
import sim.engine.SimState
import sim.portrayal.continuous.ContinuousPortrayal2D
import sim.portrayal.simple.OvalPortrayal2D
import javax.swing.JFrame
import sim.portrayal.grid.FastValueGridPortrayal2D
import sim.util.gui.ColorMap
import java.awt.Color
import sim.portrayal.inspector.TabbedInspector
import sim.portrayal.SimpleInspector
import org.jfree.data.xy.XYSeries

class ForumUI(state : ForumState) extends GUIState(state) {
	val model = state match {
		case m : ForumState => m
		case _ => throw new ClassCastException("State is not ForumState")
	}
		
	def this() = this(new ForumState(System.currentTimeMillis()))
	
	val agentPortrayal = new FastValueGridPortrayal2D();

	var display : Display2D = null
	var displayFrame : JFrame = null

	var chartFrame : JFrame = null
	
	override def getSimulationInspectedObject = state
	override def getInspector = {
		val ins = super.getInspector()
		//ins.setVolatile(true)
		//new SimpleInspector(model, this, "Parameters")
		ins
	}

	override def start() = {
		super.start()
		setup()
	}

	override def load(state : SimState) = {
		super.load(state)
		setup()
	}

	override def init(controller : Controller) = {
		super.init(controller)

		display = new Display2D(600, 600, this)
		display.setClipping(false)

		displayFrame = display.createFrame()
		displayFrame.setTitle("Online Community")
		controller.registerFrame(displayFrame)
		displayFrame.setVisible(true)

		display.attach(agentPortrayal, "Agents")
		
		initCharts(controller)
	}

	override def quit() = {
		super.quit()
		if(displayFrame != null) {
			displayFrame.dispose()
			displayFrame = null
		}
		display = null
	}

	def initCharts(controller : Controller) = {
		val chart = model.chart
    	chart.setTitle("Servers")
    	//chart.setXAxisLabel("Put the name of your charted series here")
    	//chart.setDomainAxisLabel("Time")
    	chartFrame = chart.createFrame()
    	chartFrame.setVisible(true)
    	chartFrame.pack()
    	controller.registerFrame(chartFrame)
	}
	
	def setup() = {

		 // reschedule the displayer
        display.reset();

        // redraw the display
        display.repaint();
	}
}