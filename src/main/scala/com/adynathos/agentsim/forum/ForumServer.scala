package com.adynathos.agentsim.forum

import sim.util.Bag
import sim.util.media.chart.PieChartSeriesAttributes
import scala.collection.mutable.ArrayBuffer

class ForumServer(val index : Int, val topicCount : Int, val agentCount : Int, 
		val chartSeries : PieChartSeriesAttributes) {
	val agentsByTopic = Array.fill[Int](topicCount)(0)
	val agents = new Bag
	val topicGlobalFractions = Array.fill[Double](topicCount)(0)
	
	def updateTopicFraction(topic : Int) =
		topicGlobalFractions(topic) = agentsByTopic(topic) / agentCount.toDouble
	
	def addAgent(agent : ForumAgent) = {
		val topic = agent.topic
		agents.add(agent)
		agentsByTopic(topic) += 1
		updateTopicFraction(topic)
	}
	
	def removeAgent(agent : ForumAgent) = {
		val topic = agent.topic
		agents.remove(agent)
		agentsByTopic(topic) -= 1
		updateTopicFraction(topic)
	}
	
	def topicPopularity(topic : Int) = 
		agentsByTopic(topic).toDouble / agents.size.toDouble

	def randomFriend(me : ForumAgent, state : ForumState) = {
		if(agents.size <= 1) {
			null
		} else {
			var friend : ForumAgent = null
			do {
				friend = state.randomSampleBag[ForumAgent](agents)
			} while (friend == me)
			friend
		}
	}
		
	def updateChart = {
		chartSeries.setValues(topicGlobalFractions)
	}
}
