package com.adynathos.agentsim.forum

import sim.engine.SimState
import sim.engine.Steppable
import sim.field.network.Network
import sim.util.Interval
import sim.util.Int2D
import sim.util.Bag
import scala.collection.mutable
import sim.util.media.chart.StackedBarChartGenerator
import sim.util.media.chart.SeriesAttributes
import sim.util.media.chart.PieChartSeriesAttributes

class ForumState(seed : Long, val width : Int = 50, val height : Int = 50) extends SimState(seed) {

	val agents = new scala.collection.mutable.ArrayBuffer[ForumAgent]
	val servers = new scala.collection.mutable.ArrayBuffer[ForumServer]
	val friendsNet = new Network(false)
	
	var serverCount : Int = 5
	def getServerCount = serverCount
	def setServerCount(value : Int) = {serverCount = value}
	def domServerCount : Object = new Interval(1, 30)

	var topicCount : Int = 5
	def getTopicCount = topicCount
	def setTopicCount(value : Int) = {topicCount = value}
	def domTopicCount : Object = new Interval(1, 30)

	var agentCount : Int = 100
	def getAgentCount = agentCount
	def setAgentCount(value : Int) = {agentCount = value}
	def domAgentCount : Object = new Interval(2, 5000)

	var friendCount : Int = 0
	def getFriendCount = friendCount
	def setFriendCount(value : Int) = {friendCount = value}
	def domFriendCount : Object = new Interval(0, 10)
	
	var minority : Double = 0.45
	def getMinority = minority
	def setMinority(value : Double) = minority = value
	def domMinority : Object = new Interval(0.0, 1.0)

	var switchProbability : Double = 0.05
	def getSwitchProbability = switchProbability
	def setSwitchProbability(value : Double) = switchProbability = value
	def domSwitchProbability : Object = new Interval(0.0, 1.0)

	var friendProbability : Double = 0.1
	def getFriendProbability = friendProbability
	def setFriendProbability(value : Double) = friendProbability = value
	def domFriendProbability : Object = new Interval(0.0, 1.0)
	
	var recommendationProbability : Double = 0.6
	def getRecommendationProbability = recommendationProbability
	def setRecommendationProbability(value : Double) = recommendationProbability = value
	def domRecommendationProbability : Object = new Interval(0.0, 1.0)
	
	//var series : org.jfree.data.xy.XYSeries = null
	val chart = new StackedBarChartGenerator()
	
	def toPieSeries(s :  SeriesAttributes) = s match {
		case a : PieChartSeriesAttributes => a
		case _ => throw new ClassCastException("Not PieChartSeriesAttributes")
	}
	
	def randomSampleBag[A](b : Bag) = b.get(random.nextInt(b.size)).asInstanceOf[A]
	
	def randSwitchProb = random.nextDouble() <= switchProbability
	def randRecommendationProb = random.nextDouble() <= recommendationProbability
	def randFriendProb = random.nextDouble() <= friendProbability

	def randomServerIdx = random.nextInt(serverCount)
	def randomServer = servers(randomServerIdx)
	
	override def start() = {
		super.start()

		servers.clear()
		agents.clear()
		chart.removeAllSeries()
		friendsNet.clear()
		schedule.clear()
		
		val all_zeros = Array.fill[Double](topicCount)(0.0)
		val chart_labels = (for(sidx <- 0 until topicCount) yield sidx.toString).toArray
					
		for ( sidx <- 0 until serverCount ) {
			val chart_series = toPieSeries(chart.addSeries(all_zeros, chart_labels, "Server " + sidx, null))
			val server = new ForumServer(sidx, topicCount, agentCount, chart_series)
			
			servers += server
		}
		
		for ( idx <- 0 until agentCount ) {
			val topic_id = random.nextInt(topicCount)

			val agent = new ForumAgent(topic_id)
			
			agent.moveToServer(servers(randomServerIdx))
			
			friendsNet.addNode(agent)
			
			agents += agent

			schedule.scheduleRepeating(agent)
		}
				
		schedule.scheduleRepeating(new Steppable {
			override def step(untypedState : SimState) = untypedState match {
				case state : ForumState => {
					if(state.schedule.getSteps() % 10 == 0)
					{
						// extract fractions from each server
						for(server <- servers) { server.updateChart }
						
						chart.updateChartLater(state.schedule.getSteps());
					}
				}
				case _ => {}
			}
		})
	}
}
