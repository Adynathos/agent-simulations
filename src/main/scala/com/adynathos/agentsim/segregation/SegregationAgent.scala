package com.adynathos.agentsim.segregation

import sim.engine.Steppable
import sim.engine.SimState
import sim.field.grid.Grid2D

class SegregationAgent(var x : Int, var y : Int) extends Steppable {	
	override def step(untypedState : SimState) = untypedState match {		
		case state : SegregationState => {
			val myValue = state.agentValues.get(x, y)
			val neighbours = state.agentValues.getRadialNeighbors(x, y, state.range, Grid2D.BOUNDED, false)
			val aggrFunc = state.aggregationFunctions(state.aggregationIdx)
			
			//println(myValue + " " + min + " " + max + " nsat " + dissatisfied)
			
			var min = myValue
			var max = myValue
			
			if(neighbours.objs.size > 0) {
				
				if(state.aggregationIdx == 0) {
					var avg = 0.0
					var count : Int = 0
					
					for(neighbour <- neighbours.objs) {
						if(neighbour >= 1.0) {
							avg += neighbour
							count += 1
						}
					}
					if(count > 0) {
						avg /= count.toDouble
						min = avg
						max = avg
					}
					
				} else if(state.aggregationIdx == 1) {
					for(neighbour <- neighbours.objs) {
						if(neighbour >= 1.0) {
							if(neighbour < min) min = neighbour
							else if(neighbour > max) max = neighbour
						}
					}
				}
							
				val dissatisfied = min < myValue - state.tolerance || max > myValue + state.tolerance
			
				if(dissatisfied && state.emptySpaces.nonEmpty) {
					val idx = state.random.nextInt(state.emptySpaces.size)
					val (new_x, new_y) = state.emptySpaces(idx)
					state.emptySpaces(idx) = (x, y)
					
					state.agentValues.set(x, y, state.agentValues.get(new_x, new_y))
					state.agentValues.set(new_x, new_y, myValue)
					
					x = new_x
					y = new_y
				}
			}
		}
		case _ => {}	
	}
}