package com.adynathos.agentsim.segregation

import sim.engine.SimState
import sim.field.grid.DoubleGrid2D
import sim.util.Interval
import com.adynathos.agentsim.util.PropertySet
import sim.util.Propertied
import sim.engine.Steppable
import sim.field.grid.Grid2D
import sim.util.Int2D
import scala.collection.mutable

class SegregationState(seed : Long, val width : Int = 50, val height : Int = 50) extends SimState(seed) {
	val agentValues = new DoubleGrid2D(width, height)
	val emptySpaces = new mutable.ArrayBuffer[(Int, Int)]()

	var agentDensity : Double = 0.75
	def getAgentDensity = agentDensity
	def setAgentDensity(value : Double) = {agentDensity = value}
	def domAgentDensity : Object = new Interval(0.0,1.0)

	var tolerance : Double = 0.25
	def getTolerance = tolerance
	def setTolerance(value : Double) = tolerance = value
	def domTolerance : Object = new Interval(0.0, 1.0)

	var range : Int = 2
	def getRange = range
	def setRange(value : Int) = range = value
	def domRange : Object = new Interval(0, 50)

	val aggregationNames = Array("average", "extremes")
	// Returns (min, max) or (avg, avg)
	val aggregationFunctions = Array(
		(vals : Array[Double]) => {
			//println(vals.toList)
			val avg = vals.foldLeft(0.0)(_ + _) / vals.size
			(avg, avg)
		},
		(vals : Array[Double]) => {
			vals.foldLeft((vals(0), vals(0))) {
				case ((min, max), e) => (math.min(min, e), math.max(max, e))
			}
		}
	)

	var aggregationIdx : Int = 0
	def getAggregation = aggregationIdx
	def setAggregation(idx : Int) = aggregationIdx = idx
	def domAggregation = aggregationNames

	override def start() = {
		super.start()

		for ( x <- 0 until width; y <- 0 until height ) {
			agentValues.set(x, y, random.nextDouble() match {
				case p if p < agentDensity => {
					schedule.scheduleRepeating(new SegregationAgent(x, y))
					1.0 + random.nextDouble()
				}
				case _ => {
					emptySpaces += ((x, y))
					0.0
				}
			})
		}
	}
}
