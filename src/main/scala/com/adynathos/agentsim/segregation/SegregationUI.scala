package com.adynathos.agentsim.segregation

import sim.display.Console
import sim.display.Controller
import sim.display.Display2D
import sim.display.GUIState
import sim.engine.SimState
import sim.portrayal.continuous.ContinuousPortrayal2D
import sim.portrayal.simple.OvalPortrayal2D
import javax.swing.JFrame
import sim.portrayal.grid.FastValueGridPortrayal2D
import sim.util.gui.ColorMap
import java.awt.Color
import sim.portrayal.inspector.TabbedInspector
import sim.portrayal.SimpleInspector

class SegregationUI(state : SegregationState) extends GUIState(state) {
	def this() = this(new SegregationState(System.currentTimeMillis()))
	
	val agentPortrayal = new FastValueGridPortrayal2D();
	
	var display : Display2D = null 
	var displayFrame : JFrame = null

	override def getSimulationInspectedObject = state
	override def getInspector = {
		val ins = super.getInspector()
		//ins.setVolatile(true)
		//new SimpleInspector(model, this, "Parameters")
		ins
	}
		
	override def start() = {
		super.start()
		setup()
	}
	
	override def load(state : SimState) = {
		super.load(state)
		setup()
	}
	
	override def init(controller : Controller) = {
		super.init(controller)
		
		display = new Display2D(600, 600, this)
		display.setClipping(false)
		
		displayFrame = display.createFrame()
		displayFrame.setTitle("Segregation")
		controller.registerFrame(displayFrame)
		displayFrame.setVisible(true)
		
		display.attach(agentPortrayal, "Agents")
	}
	
	override def quit() = {
		super.quit()
		if(displayFrame != null) {
			displayFrame.dispose()
			displayFrame = null
		}
		display = null
	}
	
	def model = state match {
		case m : SegregationState => m
		case _ => throw new ClassCastException("State is not SegregationState")
	}
	
	def setup() = {
		agentPortrayal.setField(model.agentValues)
		
		// Colors of agents
		agentPortrayal.setMap(new ColorMap() {
			private val colorEmpty = new Color(0.2f, 0.2f, 0.2f)
			private val colorFull = new Color(1.0f, 0.0f, 0.0f)
			
			def defaultValue = -1.0
			def getColor(level : Double) = level match {
				case x if x < 1.0 => colorEmpty
				case x if x < 2.0 => {
					val fraction = (level - 1.0f).toFloat
					new Color((1.0f - fraction).toFloat, fraction, 0.0f)
				}
				case _ => colorFull
			}
			def getAlpha(level : Double) = getColor(level).getAlpha()
			def getRGB(level : Double) = getColor(level).getRGB()
			def validLevel(level : Double) = true
		})
		
		 // reschedule the displayer
        display.reset();
                
        // redraw the display
        display.repaint();
	}
}