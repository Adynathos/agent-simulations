package com.adynathos.agentsim.students

import sim.engine.SimState
import sim.engine.Steppable
import sim.field.continuous.Continuous2D
import sim.util.Double2D

class StudentsModel(seed : Long) extends SimState(seed) {
	val yard = new Continuous2D(1.0, 100, 100)
	val center = new Double2D(yard.getWidth() * 0.5, yard.getHeight() * 0.5)
	val numStudents = 50
	val forceToSchoolMultiplier = 0.01
	val randomMultiplier = 0.1
	
	override def start() = {
		super.start()
		
		yard.clear()
		
		val students = for (i <- 1 to numStudents) yield new Student()
		
		students.foreach(s => { 
			s.setLocation(this, center.add(
				new Double2D(random.nextDouble(), random.nextDouble())))
			schedule.scheduleRepeating(s)
		})
		
	}
}

class Student extends Steppable{
	
	def location(model : StudentsModel) = model.yard.getObjectLocation(this)
	
	def setLocation(model: StudentsModel, loc : Double2D) = 
		model.yard.setObjectLocation(this, loc)
	
	override def step(state : SimState) = state match {		
		case model : StudentsModel => {
			val prevLoc = location(model)
					
			setLocation(model, prevLoc.negate())
		}
		case _ => {}	
	}
}
