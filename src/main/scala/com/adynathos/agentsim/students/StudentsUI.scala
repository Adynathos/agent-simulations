package com.adynathos.agentsim.students

import sim.display.Console
import sim.display.Controller
import sim.display.Display2D
import sim.display.GUIState
import sim.engine.SimState
import sim.portrayal.continuous.ContinuousPortrayal2D
import sim.portrayal.simple.OvalPortrayal2D
import javax.swing.JFrame

class StudentsUI(state : SimState) extends GUIState(state) {
	def this() = this(new StudentsModel(System.currentTimeMillis()))
	
	val yardPortrayal = new ContinuousPortrayal2D()

	val display : Display2D = new Display2D(600, 600, this)
	display.setClipping(false)
	
	val console = new Console(this)
	console.setPlaySleep(100)
	console.setVisible(true)
	
	var displayFrame : JFrame = null
	
	override def start() = {
		super.start()
		setup()
	}
	
	override def load(state : SimState) = {
		super.load(state)
		setup()
	}
	
	override def init(controller : Controller) = {
		displayFrame = display.createFrame()
		displayFrame.setTitle("Schoolyard Display")
		controller.registerFrame(displayFrame)
		displayFrame.setVisible(true)
		display.attach(yardPortrayal, "Yard")
	}
	
	override def quit() = {
		super.quit()
		if(displayFrame != null) {
			displayFrame.dispose()
			displayFrame = null
		}
	}
	
	def model = state match {
		case m : StudentsModel => m
		case _ => throw new ClassCastException("State is not StudentsModel")
	}
	
	def setup() = {
		yardPortrayal.setField(model.yard)
		yardPortrayal.setPortrayalForAll(new OvalPortrayal2D())
		
	}
}