\documentclass[a4paper,12pt]{article}

\usepackage{color}

%Language
\usepackage[utf8]{inputenc}

%Images
\usepackage{graphicx}
\usepackage{float}
\usepackage{subcaption}
%\renewcommand{\floatpagefraction}{.8}

%Table
% \usepackage{booktabs} % For \toprule, \midrule and \bottomrule
% \usepackage{pgfplotstable} % Generates table from .csv

% Math
\usepackage{amsmath}
\usepackage{amsfonts}
\providecommand{\e}[1]{\ensuremath{\times 10^{#1}}}

% Margins
\usepackage[top=2cm, bottom=2cm, left=2cm, right=2cm]{geometry}

%href
\usepackage[obeyspaces]{url}
\usepackage{hyperref}

%Code
\usepackage{listings}
\lstset{tabsize=4}

% Absolute positioning
\usepackage[absolute]{textpos}
\setlength{\TPHorizModule}{1px}
\setlength{\TPVertModule}{1px}

%% Images with a different path to the parent latex file can
%% be accessed with the `import' package (which may need to be
%% installed) using
%%   \usepackage{import}
%% in the preamble, and then including the image with
%%   \import{<path to file>}{<filename>.pdf_tex}
%% Alternatively, one can specify
%%   \graphicspath{{<path to file>/}}
\usepackage{import}

% Header
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Schelling segregation for online communities}
\rhead{Krzysztof Lis}

\begin{document}

%\title{}
%\author{Krzysztof Lis}
%\date{}
%\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INTRODUCTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Schelling segregation for online communities}

\subsubsection*{Properties of online communities}
In a previous assignment, I have argued that the Schelling segregation model
is not a fitting representation of spatial migrations, as economic factors
may be the dominant force in that phenomenon.
However, if we analyse internet fora, social media groups, 
any online communities formed for the purpose of discussion,
we will find that many of Schelling model's assumptions are valid:
\begin{itemize}
	\item \textit{Separation into groups (types of agents)}.
	In this context, the groups would be fans of a particular
	topic - film, book or game series, a shared hobby, music genre - 
	who want to discuss this topic.
	
	\item \textit{Members of a community are its most significant feature}
	People come to the discussion group to interact with other members,
	therefore their enjoyment depends nearly solely on the other people
	in the community.
	In the case of a house, one may not even interact with their neighbours
	and like the location for having a nice view or convenient transport.
	
	\item \textit{Being the minority makes people leave}.
	If the majority of discussions on a forum is about a topic
	the person is not interested in, they are likely to look
	for a different community.
	
	\item \textit{Ease of migration}.
	Unlike moving to a new house, registering in a new internet
	community is simple, quick and carries no cost.
\end{itemize}
I extend the model by adding friendships,
as indicated by the fact that many internet communities

The model is described in detail in the next section.

\subsubsection*{Model specification}

There are $T$ distinct topic that agents can be interested in,
each agent is interested in exactly one topic.
There are $S$ servers (fora) where the discussions take place.
Each agent is at any time registered on exactly one server.
Additionally, agents form friendships, which can be modelled
as an unidirectional network: agents are the nodes,
friendship is indicated by presence of an edge.
The maximal number of friends per agent is $F$.
This can be expressed using networks described in figure~\ref{fig:networks}.

During each activation, an agent has a probability $P_f$ to
choose a new friend from the users of their current server.
If they have more than $F$ friends, random friend is removed.

An agent is not satisfied if less than $M \cdot (\mbox{number of agents on server})$
share their topic, $0 \leq M \leq 1$.
When an agent is not satisfied, they have a $P_s$ probability to decide to
move during each activation.

When an agent moves, they have probability $P_r$ to decide to ask a friend
for advice. In that case, they will choose a random friend and move to
their server (this may result in staying at the same server).
Otherwise, they will choose a random server to move to.

\begin{figure}
	\centering
	\def\svgwidth{\columnwidth}
	\import{imgs/}{networks.pdf_tex}
	\caption{\label{fig:networks}
		Participation of agents in communities (servers) can be represented
		on a bipartite graph, with each agent having an edge pointing to
		the chosen server (left).
		Friendships are represented as edges of an undirected graph of
		agents (right).
	}
\end{figure}

\subsubsection*{Hypothesis}
I will try to answer the following questions about the new model:
\begin{itemize}
	\item Does segregation happen in the new model and if 
	so what are the characteristics of the segregation?
	\item What is the influence of friendships - do they
	strengthen or weaken the segregation?
\end{itemize}

\subsubsection*{Instantiation and observations}
I have implemented the proposed model using the MASON framework.
Agents are activated in random order.
The composition of agents by topic in each server is visualized.
I have made the following observations:
\begin{itemize}
	\item When there are more servers than topics,
	each topic can easily find its own server and segmentation
	always occurs, as seen in figure~\ref{fig:sim_many_servers}.
	
	\item When there are fewer servers than topics
	and actors insist on being nearly the majority 
	(at least $M = 0.45$ of the community), 	
	usually no topic breaks this threshold and
	no clusters are formed, see figure~\ref{fig:sim_many_topics}.
	
	\item When agents tolerate being the $M = 0.3$ minority,
	it is easier for the clusters to form.
	The clusters may either all coexist (figure ~\ref{fig:sim_coexistence_all})
	or leave one topic scattered (figure~\ref{fig:sim_coexistence_with_outsider}).	
\end{itemize}

In this model, clusters are able to form only for some range
of \textit{required majority} $M$ values. If agents want to be the dominant majority,
they try to move constantly and clusters are not created.

The effect of friendship networks is not noticable.
Since the friends are usually on the same server,
following friends may act just as a reduction of probability that the agent moves 
to a different server in a given activation,
but does not prevent the agent from being dissatisfied and
moving eventually later.
	
	
\begin{figure}
	\centering

	\begin{subfigure}[b]{0.45\columnwidth}
		\includegraphics[width=\columnwidth]{imgs/sim_many_servers}
		\caption{\label{fig:sim_many_servers}
			Simulation result for $T = 4$, $S = 5$, $M = 0.45$.
			Different colors symbolize different topics.
			When there are fewer topics than servers,
			segmentation always occurs.
		}
    \end{subfigure}
    \quad
    \begin{subfigure}[b]{0.45\columnwidth}
		\includegraphics[width=\columnwidth]{imgs/sim_many_topics}
		\caption{\label{fig:sim_many_topics}
			Simulation result for $T = 6$, $S = 5$, $M = 0.45$.
			When there are more topics than servers,
			and agents want to be in the majority,
			segregation is unlikely to occur.
		}
    \end{subfigure}	
    
	\begin{subfigure}[b]{0.45\columnwidth}
		\includegraphics[width=\columnwidth]{imgs/sim_coexistence_with_outsider}
		\caption{\label{fig:sim_coexistence_with_outsider}
			Simulation result for $T = 6$, $S = 5$, $M = 0.3$.
			Acceptance of being a smaller minority helps
			formation of clusters.
			One topic is left scattered even after many iterations.
		}
    \end{subfigure}
    \quad
    \begin{subfigure}[b]{0.45\columnwidth}
		\includegraphics[width=\columnwidth]{imgs/sim_coexistence_all}
		\caption{\label{fig:sim_coexistence_all}
			Simulation result for $T = 6$, $S = 5$, $M = 0.3$.
			Acceptance of being a smaller minority helps
			formation of clusters and also
			allows the clusters to coexist.
		}
    \end{subfigure}	    
    
	\caption{\label{fig:results}
		Simulation results.
		Bars represent the agent distribution on servers.
		Different colors symbolize agents interested in different topics.
	}
\end{figure}

\end{document}

