\documentclass[a4paper,12pt]{article}

\usepackage{color}

%Language
\usepackage[utf8]{inputenc}

%Images
\usepackage{graphicx}
\usepackage{float}
\usepackage{subcaption}
%\renewcommand{\floatpagefraction}{.8}

%Table
% \usepackage{booktabs} % For \toprule, \midrule and \bottomrule
% \usepackage{pgfplotstable} % Generates table from .csv

% Math
\usepackage{amsmath}
\usepackage{amsfonts}
\providecommand{\e}[1]{\ensuremath{\times 10^{#1}}}

% Margins
\usepackage[top=2cm, bottom=2cm, left=2cm, right=2cm]{geometry}

%href
\usepackage[obeyspaces]{url}
\usepackage{hyperref}

%Code
\usepackage{listings}
\lstset{tabsize=4}

% Absolute positioning
\usepackage[absolute]{textpos}
\setlength{\TPHorizModule}{1px}
\setlength{\TPVertModule}{1px}

% Header
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Evaluation of Schelling segregation model}
\rhead{Krzysztof Lis}

\begin{document}

%\title{}
%\author{Krzysztof Lis}
%\date{}
%\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INTRODUCTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Evaluation of Schelling segregation model}
\subsubsection*{In the context of spatial segregation and migration}
It is tempting to use the Schelling segregation model as an explanation of spatial
segregation of a city's inhabitants in regard to some feature, for example race.
I believe this line of reasoning is flawed, because the following assumptions made by
the model are not valid in the phenomenon of spatial migration:
\begin{itemize}
	\item 
	\textit{The features of neighbours are the dominant factor governing migration.}
	However, people often migrate in to pursue employment, education or more
	comfortable living conditions. 
	These factors depend on the availability of economic or academic facilities in the 
	region, as well as quality of life and transportation in the region, not on the neighbourhood composition.
	
	\item 
	\textit{People can freely migrate to any unoccupied space in the region}
	As described in the previous point, some places are more attractive than others.
	High demand leads to increased price, and we can observe how living in the city
	center is expensive.
	A person's place of living may strongly depend on their financial situation.
	The clustering observed in reality may be an effect of difference in wealth between
	the groups, and not of their preference.
	
	\item
	\textit{People interact mostly with their neighbours.}
	Especially in a city, many people work in a different area than they live.
	Their social interactions may therefore not correspond to the geographical
	neighbourhood of their house.

	\item
	\textit{No outward/inward migration, constant group of people, constant population density}
	In reality people migrate in and out of the city, for example many move from nearby villages to work in the city. The model does not simulate any migrations.	
\end{itemize}
\subsubsection*{In other situations}
The model may however be more valid in different areas of life, where the assumptions
could be satisfied.
The general concept that people prefer to interact with those similar to them follows
the everyday experience.
If we replace proximity-based-interactions with the ability to freely join one of
many groups, and keep the rule that no one wants to be in a minority in their group,
we can expect the group division to follow the difference in the chosen feature.

For example, let us imagine that employees of a company are asked to perform a list of
projects in groups. Some of them use technology $A$ and others use $B$.
If the majority of workers in some project use $A$, then the project will run on $A$.
The employees proficient in $B$ will probably prefer to migrate to a another project which uses the technology they are more comfortable with.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MOD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Modification of the model}
\subsubsection*{Model description}
I have implemented a modification of the Schelling segregation model, such that:
\begin{itemize}
	\item Agents reside on a grid, like in the original model, with density $\rho$.

	\item Instead of being in one of the groups, an agent has a continuous
	value $v \in \mathbb{R},\; 1 \leq v \leq 2$. The value is randomly
	assigned at initialization.

	\item On each step of the simulation, an agent calculates the average value
	$\bar{v}$ over all neighbours in radius $d$.
	
	\item If the difference $|v - \bar{v}|$ is greater than the 
	\textit{tolerance} $T$, the agent moves to a random free space on the grid.
\end{itemize}
The program for this simulation can be found in the \verb|simulation| directory
and the code in the \verb|source| directory. 
The program uses the MASON simulation framework.

\subsubsection*{Observations}
\begin{itemize}
	\item The simulation needs much more steps to enter a stable state than the
	original model. For many combinations of the parameters no stable state is reached.
	
	\item For $\rho = 0.75, T = 0.3, d=2$, clusters or high or low values are formed,
	and the medium valued agents are spread in between the clusters with low density.
	A stable state is reached.
	An example simulation result is shown in Figure~\ref{fig:sim_t_03_d_2}.

	\item For $\rho = 0.75, T = 0.2, d=2$, clusters are also formed, but their
	edges may be filled with medium value agents and then the very high and very low
	valued agents are outside the clusters and they are never satisfied.
	An example simulation result is shown in Figure~\ref{fig:sim_t_02_d_2}.

\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{imgs/T03d2}
	\caption{\label{fig:sim_t_03_d_2}
		Simulation for parameters $\rho = 0.75, T = 0.3, d=2$. A stable state was
		reached, with clusters of similar values.
	}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{imgs/T02d2b}
	\caption{\label{fig:sim_t_02_d_2}
		Simulation for parameters $\rho = 0.75, T = 0.2, d=2$. 
		The state was not stable after 10000 steps.
		The medium-valued agents on the edge of clusters prevent 
		extreme-valued agents from attaching to clusters.
		The agents outside of clusters are not stable.
	}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LINKS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \begin{thebibliography}{9}

% % \bibitem{pe_lecture_3}
% % 	Wojciech Dominik,
% % 	\emph{Pracownia Fizyczna i Elektroniczna, wykład 3}.
% % 	\url{http://pe.fuw.edu.pl/fizelektr/pliki/wyklad 3 PFE2014 dioda.pdf},
% % 	2014-03-05.
% \end{thebibliography}

\end{document}

